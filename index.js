import React from "react";
import ReactDOM  from "react-dom";


function Missed(){
    return <h1>Missed</h1>;
}
function Made(){
    return <h1>Goal</h1>;
}
function Goal(props){
    const isGoal= props.isGoal;
if(isGoal){
    return<Made/>;
}
return<Missed/>;
}
ReactDOM.render(<Goal isGoal={false}/>,document.getElementById('root'));
